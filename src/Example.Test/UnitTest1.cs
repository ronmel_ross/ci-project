using System;
using Xunit;

namespace Example.Test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            //Assert.True(true);get { 
            string retVal = Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER");
            string retVal2 = Environment.GetEnvironmentVariable("AGENT_NAME");
            return (
                (String.Compare(retVal, Boolean.TrueString, ignoreCase: true) == 0)  
                || 
                (String.IsNullOrWhiteSpace(retVal2) == false));            
        }
    }
}

// [Theory]
// [InlineData(-1)]
// [InlineData(0)]
// [InlineData(1)]
// public void IsPrime_ValuesLessThan2_ReturnFalse(int value)
// {
//     var result = _primeService.IsPrime(value);
    
//     Assert.False(result, $"{value} should not be prime");
// }

// [DataTestMethod]
// [DataRow(-1)]
// [DataRow(0)]
// [DataRow(1)]
// public void IsPrime_ValuesLessThan2_ReturnFalse(int value)
// {
//     var result = _primeService.IsPrime(value);

//     Assert.IsFalse(result, $"{value} should not be prime");
// }